Here is an example of using this library:

```
    {Pub, Priv} = sign:new_key(),
    {Pub2, Priv2} = sign:new_key(),
    Val = <<"1234">>,
    Binary = <<2,3,4>>,
    true = encryption:bin_dec("abc", encryption:bin_enc("abc", Val)) == Val,
    true = encryption:bin_dec("abc", encryption:bin_enc("abc", Binary)) == Binary,
    Record = {f, Binary},
    Sig = encryption:get_msg(encryption:send_msg(Record, Pub2, Pub, Priv), Priv2),
    true = encryption:msg(Sig) == Record.
```